**Crie um usuario IAM com permissao de administrator a sua conta aws, ex: eks-admin. Dê a permissao de administrador.**

```
https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/getting-started_create-admin-group.html
```

**Crie usuário no linux**

```
sudo adduser eks-<conta>-<env>
ex: sudo adduser eks-poc-dev
```

IMPORTANTE!! DE PERMISSÃO SUDO (ROOT) AO USUÁRIO QUE ACABOU DE SER CRIADO 

**Configure as credenciais no usuario que acabou de ser criado**

```
sudo su - eks-<conta>-<env>
aws configure 
ACCESS_KEY_ID....
SECRET_KEY....
AWS_REGION....
```

**Exportando variáveis que serão usadas posteriomente**

```
echo 'export LBC_VERSION="v2.2.0"' >>  ~/.bash_profile
. ~/.bash_profile
```

```
echo 'export ALB_INGRESS_VERSION="v2.2.0"' >> ~/.bash_profile
echo export ACCOUNT_ID=$(aws sts get-caller-identity --output text --query Account) >> ~/.bash_profile
echo export AWS_REGION=us-east-1 >> ~/.bash_profile
. ~/.bash_profile
```

**Se necessário, acesse o arquivo .bash_profile e altere a linha: export AWS_REGION='<sua-regiao>'**

**Criando usuario IAM na AWS, para utilização do acesso ao cluster**

```
aws iam create-group --group-name k8sAdmin
aws iam create-user --user-name $USER
aws iam add-user-to-group --group-name k8sAdmin --user-name $USER
```


**Criando policies e roles necessárias para o usuário adm e permissão do ingress e route53**

```
POLICY=$(echo -n '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":"arn:aws:iam::'; echo -n "$ACCOUNT_ID"; echo -n ':root"},"Action":"sts:AssumeRole","Condition":{}}]}')
```

```
aws iam create-role --role-name k8sAdmin --description "Kubernetes administrator role (for AWS IAM Authenticator for Kubernetes)." --assume-role-policy-document "$POLICY" --output text --query 'Role.Arn'
```

```
ADMIN_GROUP_POLICY=$(echo -n '{ "Version": "2012-10-17", "Statement": [ { "Sid": "AllowAssumeOrganizationAccountRole", "Effect": "Allow", "Action": "sts:AssumeRole", "Resource": "arn:aws:iam::'; echo -n "$ACCOUNT_ID"; echo -n ':role/k8sAdmin" } ] }')
```

```
aws iam put-group-policy --group-name k8sAdmin --policy-name k8sAdmin-policy --policy-document "$ADMIN_GROUP_POLICY"
```

```
curl -o iam-policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/${ALB_INGRESS_VERSION}/docs/install/iam_policy.json
```

```
aws iam create-policy --policy-name ALBIngressControllerIAMPolicy --policy-document file://iam-policy.json
```

```
echo export PolicyARN=$(aws iam list-policies --query 'Policies[?PolicyName==`ALBIngressControllerIAMPolicy`].Arn' --output text) >> ~/.bash_profile
. ~/.bash_profile
```

**Referencia**

- https://www.eksworkshop.com/beginner/091_iam-groups/intro/

- https://www.eksworkshop.com/beginner/130_exposing-service/ingress_controller_alb/





